User.create!(name: 'Renzo Solis', email: 'rensoliscastillo@gmail.com',
             password: 'password', password_confirmation: 'password',
             admin: true, activated: true, activated_at: Time.zone.now)

# USERS
99.times do |n|
  name = Faker::Name.name
  email = "example-#{n + 1}@railstutorial.org"
  password = 'password'
  User.create!(name: name, email: email, password: password,
               password_confirmation: password, activated: true,
               activated_at: Time.zone.now)
end

# MICROPOSTS
users = User.order(:created_at).take(6)
50.times do |n|
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

# FOLLOWING RELATIONSHIPS
users = User.all
user = User.first
following = users[2..50]
followed = users[3..40]
following.each { |followed| user.follow(followed) }
followed.each { |follower| follower.follow(user) }
