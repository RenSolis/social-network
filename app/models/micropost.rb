# Micropost Model
class Micropost < ApplicationRecord
  mount_uploader :picture, PictureUploader

  MAX_LENGTH_CONTENT = 140

  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: MAX_LENGTH_CONTENT }
  validate :picture_size

  belongs_to :user

  default_scope -> { order(created_at: :desc) }

  private

  # Validates the size of an uploaded picture
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, 'should be less than 5MB')
    end
  end
end
