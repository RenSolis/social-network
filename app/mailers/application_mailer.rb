# Application Mailer
class ApplicationMailer < ActionMailer::Base
  default from: 'Renzo Solis <rensoliscastillo@gmail.com>'
  layout 'mailer'
  append_view_path Rails.root.join('app', 'views', 'mailers')
end
